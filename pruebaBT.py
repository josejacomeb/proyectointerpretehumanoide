#!/usr/bin/env python3

from PyQt5.QtBluetooth import *
from PyQt5.QtCore import *

class BTServer():
    '''
        Class based on: https://doc.qt.io/qt-5/qtbluetooth-btchat-example.html
    '''
    def __init__(self):
        self.startServer()
    def startServer(self, localAdapter=QBluetoothAddress()):
        self.rfcommServer = QBluetoothServer(QBluetoothServiceInfo.RfcommProtocol)
        self.rfcommServer.newConnection.connect(self.clientConnected)
        self.result = self.rfcommServer.listen(localAdapter)
        self.clientSockets = []
        if not self.result:
            print("Cannot bind chat server to: " + localAdapter.toString())
        self.serviceInfo = QBluetoothServiceInfo()
        self.serviceInfo.setAttribute(QBluetoothServiceInfo.ServiceName, "Bt Chat Server")
        self.serviceInfo.setAttribute(QBluetoothServiceInfo.ServiceDescription,
                         "Example bluetooth chat server")
        self.serviceInfo.setAttribute(QBluetoothServiceInfo.ServiceProvider, "qt-project.org")
        self.serviceUuid = QBluetoothUuid("e8e10f95-1a70-4b27-9ccf-02010264e9c8")
        self.serviceInfo.setServiceUuid(QBluetoothUuid(self.serviceUuid))
        self.PublicBrowseGroup = QVariant(QBluetoothUuid(QBluetoothUuid.PublicBrowseGroup))
        self.serviceInfo.setAttribute(QBluetoothServiceInfo.BrowseGroupList, self.PublicBrowseGroup)
        self.protocol = [QVariant(QBluetoothUuid(QBluetoothUuid.L2cap))]
        self.protocolDescriptorList = [QVariant(QBluetoothUuid(QBluetoothUuid.L2cap)),
                                       QVariant(QBluetoothUuid(QBluetoothUuid.Rfcomm)),
                                       QVariant(int(self.rfcommServer.serverPort()))]
        print(self.protocolDescriptorList)
        self.serviceInfo.setAttribute(QBluetoothServiceInfo.ProtocolDescriptorList,
                         self.protocolDescriptorList);
        self.serviceInfo.registerService(localAdapter)

    def clientConnected(self):
        self.socket = self.rfcommServer.nextPendingConnection()
        if not self.socket:
            return
        self.rfcommServer.readyRead.connect(self.readSocket)
        self.rfcommServer.disconnected.connect(self.clientDisconnected)
        self.clientSockets.append(self.socketBT)
        print(self.socketBT.peerName())
    def readSocket(self):
        print("Socket data")
    def clientDisconnected(self):
        print("Client Disconected")
