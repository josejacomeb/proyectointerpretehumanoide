'''
    Programa para controlar la Pantalla OLED a través del programa
'''
import platform
from PIL import Image
import os
if platform.machine() == 'armv7l':
    import Adafruit_GPIO.SPI as SPI
    import Adafruit_SSD1306
import time

class ControlPantalla():
    def __init__(self, RST=5 , delay = 0.5): #GPIO 7 y 12
        self.letras = {
            "a": os.getcwd() + "/imagenes/aei.ppm",
            "b": os.getcwd() + "/imagenes/bmp.ppm",
            "c": os.getcwd() + "/imagenes/cdgnknstxyz.ppm",
            "d": os.getcwd() + "/imagenes/cdgnknstxyz.ppm",
            "e": os.getcwd() + "/imagenes/aei.ppm",
            "f": os.getcwd() + "/imagenes/fv.ppm",
            "g": os.getcwd() + "/imagenes/cdgnknstxyz.ppm",
            "i": os.getcwd() + "/imagenes/aei.ppm",
            "j": os.getcwd() + "/imagenes/j.ppm",
            "k": os.getcwd() + "/imagenes/cdgnknstxyz.ppm",
            "l": os.getcwd() + "/imagenes/l.ppm",
            "m": os.getcwd() + "/imagenes/bmp.ppm",
            "n": os.getcwd() + "/imagenes/cdgnknstxyz.ppm",
            "ñ": os.getcwd() + "/imagenes/cdgnknstxyz.ppm",
            "o": os.getcwd() + "/imagenes/o.ppm",
            "p": os.getcwd() + "/imagenes/bmp.ppm",
            "q": os.getcwd() + "/imagenes/qw.ppm",
            "r": os.getcwd() + "/imagenes/r.ppm",
            "s": os.getcwd() + "/imagenes/cdgnknstxyz.ppm",
            "t": os.getcwd() + "/imagenes/cdgnknstxyz.ppm",
            "u": os.getcwd() + "/imagenes/u.ppm",
            "v": os.getcwd() + "/imagenes/fv.ppm",
            "w": os.getcwd() + "/imagenes/qw.ppm",
            "x": os.getcwd() + "/imagenes/cdgnknstxyz.ppm",
            "y": os.getcwd() + "/imagenes/cdgnknstxyz.ppm",
            "z": os.getcwd() + "/imagenes/cdgnknstxyz.ppm",
            "ingenieria": os.getcwd() + "/imagenes/ingenieriamecatronica.ppm"
        }
        if platform.machine() == 'armv7l':
            self.disp = Adafruit_SSD1306.SSD1306_128_64(rst=RST)
            # Initialize library.
            self.disp.begin()

            # Clear display.
            self.disp.clear()
            self.disp.display()
            self.image = Image.open(self.letras['ingenieria']).convert('1')
            self.disp.image(self.image)
            self.disp.display()

        self.delay = delay
        self.anterior = 'a'
        print("Finalizo configurar OLED")
    def mostrarLetra(self, letra):

        if letra.lower() == 'h': #La h no tiene movimiento bocal
            letra = self.anterior
        self.image = Image.open(self.letras[letra.lower()]).convert('1')
        if platform.machine() == 'armv7l':
            self.disp.image(self.image)
            self.disp.display()
        else:
            print("Letra: " + letra)
        self.anterior = letra
        time.sleep(self.delay)
