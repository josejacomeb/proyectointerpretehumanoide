#!/usr/bin/env python3

from PyQt5.QtWidgets import QDialog
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from setearservos import Ui_Dialog
import imagenes_rc
import json #Como diccionario y lista de Python
from controlservos import *

objetoControlServos = ControlServos(delay=0.1) #Inicializo el objeto
class accionServos(QThread):
    senalFin =  pyqtSignal(str)
    def __init__(self, pasos=10):
        QThread.__init__(self)
        self.pasos = pasos

    def run(self):
        print("-----* Inicializa while *---------")
        self.senalFin.emit("inicio")
        self.finalizaracciones = False
        while self.finalizaracciones == False:
            self.finalizaracciones = True
            movimientoDeseado = {
            'nombre': 'MovimientoDeseado',
            'BicepIzq': 0,
            'DeltoideIzq': 0,
            'AntebrazoIzq': 0,
            'CodoIzq': 0,
            'MunecaIzq': 0,
            'IndiceIzq': 0,
            'MedioIzq': 0,
            'AnularIzq': 0,
            'MeniqueIzq': 0,
            'PulgarIzq': 0,
            'BicepDer': 0,
            'DeltoideDer': 0,
            'AntebrazoDer': 0,
            'CodoDer': 0,
            'MunecaDer': 0,
            'IndiceDer': 0,
            'MedioDer': 0,
            'AnularDer': 0,
            'MeniqueDer': 0,
            'PulgarDer': 0
            }

            for i in self.datos:
                if i['finalizado'] == False:
                    i['actual'] += i['incremento']
                movimientoDeseado[i['movimiento']] = i['actual']
                if int(i['actual']) == int(i['deseado']):
                    i['finalizado'] = True
                else:
                    if int(i['actual']) <= int(i['deseado'] + abs(i['incremento']/2)) and int(i['actual']) >= int(i['deseado']- abs(i['incremento']/2)):
                        i['finalizado'] = True
                self.finalizaracciones &= i['finalizado']
            if ~self.finalizaracciones:
                objetoControlServos.moverGrupo(movimientoDeseado)
            else:
                print(self.datos)
                print("***Finalizado***")
            print("Finalizado? " + str(self.finalizaracciones))
        print("-----* Fin while *---------")
        self.senalFin.emit("fin")
    def __del__(self):
        self.wait()

    def inicializarAcciones(self, deseado, actual, default = False):
        '''
        Inicializa el vector de datos para la ejecución en el hilo Principal
        '''
        if default:
            time.sleep(objetoControlServos.delay*10)
        self.datos = []
        llaves = deseado.keys()
        for x in llaves:
            if x != 'nombre':
                incrementoservos = 0
                if (deseado[x] - actual[x])/self.pasos <= 2:
                    incrementoservos = (deseado[x] - actual[x])
                else:
                    incrementoservos = (deseado[x] - actual[x])/self.pasos
                diccionario = {
                'movimiento': x,
                'actual': actual[x],
                'deseado': deseado[x],
                'finalizado': False,
                'incremento': incrementoservos
                }
                self.datos.append(diccionario)

class accionarServos(QThread):
    senalFin =  pyqtSignal(str)
    def __init__(self, pasos=10):
        QThread.__init__(self)
    def run(self):
        objetoControlServos.moverIndividual(self.ID, int(self.angulo))
        self.senalFin.emit("fin")
    def valores(self, ID, angulo):
        self.ID = ID
        self.angulo = angulo


class Ui_Servos(QDialog):
    datos = []
    movimiento1 = {
    'BicepIzq': 0,
    'DeltoideIzq': 0,
    'AntebrazoIzq': 0,
    'CodoIzq': 0,
    'MunecaIzq': 0,
    'IndiceIzq': 0,
    'MedioIzq': 0,
    'AnularIzq': 0,
    'MeniqueIzq': 0,
    'PulgarIzq': 0,
    'BicepDer': 0,
    'DeltoideDer': 0,
    'AntebrazoDer': 0,
    'CodoDer': 0,
    'MunecaDer': 0,
    'IndiceDer': 0,
    'MedioDer': 0,
    'AnularDer': 0,
    'MeniqueDer': 0,
    'PulgarDer': 0
    }
    movimiento2 = movimiento1
    movimiento3 = movimiento1
    def __init__(self):
        super(Ui_Servos, self).__init__()
        # Set up the user interface from Designer.
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        #Carga del archivo JSON
        self.inicializarAcciones()
        #Codigo añadido
        self.ui.botonGuardar.clicked.connect(self.guardar)
        self.ui.botonSalir.clicked.connect(self.reject)
        self.ui.botonEliminar.clicked.connect(self.eliminarAcciones)
        self.ui.selectorAcciones.currentTextChanged.connect(self.cambiarValor)
        self.ui.contBicepIzq.valueChanged.connect(self.BicepIzq)
        self.ui.contDeltoideIzq.valueChanged.connect(self.DeltoideIzq)
        self.ui.contAntebrazoIzq.valueChanged.connect(self.AntebrazoIzq)
        self.ui.contCodoIzq.valueChanged.connect(self.CodoIzq)
        self.ui.contMunecaIzq.valueChanged.connect(self.MunecaIzq)
        self.ui.contIndiceIzq.valueChanged.connect(self.IndiceIzq)
        self.ui.contMedioIzq.valueChanged.connect(self.MedioIzq)
        self.ui.contAnularIzq.valueChanged.connect(self.AnularIzq)
        self.ui.contMeniqueIzq.valueChanged.connect(self.MeniqueIzq)
        self.ui.contPulgarIzq.valueChanged.connect(self.PulgarIzq)
        self.ui.contBicepDer.valueChanged.connect(self.BicepDer)
        self.ui.contDeltoideDer.valueChanged.connect(self.DeltoideDer)
        self.ui.contAntebrazoDer.valueChanged.connect(self.AntebrazoDer)
        self.ui.contCodoDer.valueChanged.connect(self.CodoDer)
        self.ui.contMunecaDer.valueChanged.connect(self.MunecaDer)
        self.ui.contIndiceDer.valueChanged.connect(self.IndiceDer)
        self.ui.contMedioDer.valueChanged.connect(self.MedioDer)
        self.ui.contAnularDer.valueChanged.connect(self.AnularDer)
        self.ui.contMeniqueDer.valueChanged.connect(self.MeniqueDer)
        self.ui.contIndiceDer.valueChanged.connect(self.IndiceDer)
        self.ui.contMedioDer.valueChanged.connect(self.MedioDer)
        self.ui.contAnularDer.valueChanged.connect(self.AnularDer)
        self.ui.contMeniqueDer.valueChanged.connect(self.MeniqueDer)
        self.ui.contPulgarDer.valueChanged.connect(self.PulgarDer)
        self.ui.selectorMovimiento.currentTextChanged.connect(self.cambiarMovimiento)
        self.threadServos = accionarServos(self.ui)
        self.threadServos.senalFin.connect(self.finalizarThread)
        self.threadTodosServos = accionServos()
        self.threadTodosServos.senalFin.connect(self.finalizarThread)
        datosdeseados = self.obtenerValorJSON("Default")

        if ~self.threadTodosServos.isRunning():
            print(datosdeseados)
            self.threadTodosServos.inicializarAcciones(datosdeseados.movimiento1, objetoControlServos.posicionactual, True)
            self.threadTodosServos.start()
    def obtenerValorJSON(self, valor):
        print("******* Nuevo valor *******")
        with open('angulosservos.json', 'r') as f:
            data = f.read()
        self.datos = json.loads(data)
        datosdeseados = {}
        for x in self.datos:
            print("Bool: " + str(x.get("nombre").lower() == valor.lower()) + " vector: " +  str(x.get("nombre").lower()) + " " + str(type(x.get("nombre").lower())) + " valor: " + str(valor.lower())+ " " + str(type(valor.lower())))
            if x.get("nombre").lower() == valor.lower():
                datosdeseados = x
                break
        return datosdeseados

    def finalizarThread(self, valor):
        if valor == "fin":
            self.threadTodosServos.wait()
    def BicepIzq(self, i):
        self.cambiarListas('BicepIzq', i)
        if ~self.threadServos.isRunning():
            self.ui.movMax.setText(str(self.ui.contBicepIzq.maximum()))
            self.ui.movMin.setText(str(self.ui.contBicepIzq.minimum()))
            self.threadServos.valores("BicepIzq", i)
            self.ui.ultimoMov.setText("BicepIzq: " + str(i))
            self.threadServos.start()
    def DeltoideIzq(self, i):
        self.cambiarListas('DeltoideIzq', i)
        if ~self.threadServos.isRunning():
            self.threadServos.valores("DeltoideIzq", i)
            self.ui.movMax.setText(str(self.ui.contDeltoideIzq.maximum()))
            self.ui.movMin.setText(str(self.ui.contDeltoideIzq.minimum()))
            self.ui.ultimoMov.setText("DeltoideIzq: " + str(i))
            self.threadServos.start()
    def AntebrazoIzq(self, i):
        self.cambiarListas('AntebrazoIzq', i)
        if ~self.threadServos.isRunning():
            self.threadServos.valores("AntebrazoIzq", i)
            self.ui.movMax.setText(str(self.ui.contAntebrazoIzq.maximum()))
            self.ui.movMin.setText(str(self.ui.contAntebrazoIzq.minimum()))
            self.ui.ultimoMov.setText("AntebrazoIzq: " + str(i))
            self.threadServos.start()
    def CodoIzq(self, i):
        self.cambiarListas('CodoIzq', i)
        if ~self.threadServos.isRunning():
            self.threadServos.valores("CodoIzq", i)
            self.ui.movMax.setText(str(self.ui.contCodoIzq.maximum()))
            self.ui.movMin.setText(str(self.ui.contCodoIzq.minimum()))
            self.ui.ultimoMov.setText("CodoIzq: " + str(i))
            self.threadServos.start()
    def MunecaIzq(self, i):
        self.cambiarListas('MunecaIzq', i)
        if ~self.threadServos.isRunning():
            self.threadServos.valores("MunecaIzq", i)
            self.ui.movMax.setText(str(self.ui.contMunecaIzq.maximum()))
            self.ui.movMin.setText(str(self.ui.contMunecaIzq.minimum()))
            self.ui.ultimoMov.setText("MunecaIzq: " + str(i))
            self.threadServos.start()
    def IndiceIzq(self, i):
        self.cambiarListas('IndiceIzq', i)
        if ~self.threadServos.isRunning():
            self.threadServos.valores("IndiceIzq", i)
            self.ui.movMax.setText(str(self.ui.contIndiceIzq.maximum()))
            self.ui.movMin.setText(str(self.ui.contIndiceIzq.minimum()))
            self.ui.ultimoMov.setText("IndiceIzq: " + str(i))
            self.threadServos.start()
    def MedioIzq(self, i):
        self.cambiarListas('MedioIzq', i)
        if ~self.threadServos.isRunning():
            self.threadServos.valores("MedioIzq", i)
            self.ui.movMax.setText(str(self.ui.contMedioIzq.maximum()))
            self.ui.movMin.setText(str(self.ui.contMedioIzq.minimum()))
            self.ui.ultimoMov.setText("MedioIzq: " + str(i))
            self.threadServos.start()
    def AnularIzq(self, i):
        self.cambiarListas('AnularIzq', i)
        if ~self.threadServos.isRunning():
            self.threadServos.valores("AnularIzq", i)
            self.ui.movMax.setText(str(self.ui.contAnularIzq.maximum()))
            self.ui.movMin.setText(str(self.ui.contAnularIzq.minimum()))
            self.ui.ultimoMov.setText("AnularIzq: " + str(i))
            self.threadServos.start()
    def MeniqueIzq(self, i):
        self.cambiarListas('MeniqueIzq', i)
        if ~self.threadServos.isRunning():
            self.threadServos.valores("MeniqueIzq", i)
            self.ui.movMax.setText(str(self.ui.contMeniqueIzq.maximum()))
            self.ui.movMin.setText(str(self.ui.contMeniqueIzq.minimum()))
            self.ui.ultimoMov.setText("MeniqueIzq: " + str(i))
            self.threadServos.start()
    def PulgarIzq(self, i):
        self.cambiarListas('PulgarIzq')
        if ~self.threadServos.isRunning():
            self.threadServos.valores("PulgarIzq", i)
            self.ui.movMax.setText(str(self.ui.contPulgarIzq.maximum()))
            self.ui.movMin.setText(str(self.ui.contPulgarIzq.minimum()))
            self.ui.ultimoMov.setText("PulgarIzq: " + str(i))
            self.threadServos.start()
    def BicepDer(self, i):
        self.cambiarListas('BicepDer', i)
        if ~self.threadServos.isRunning():
            self.threadServos.valores("BicepDer", i)
            self.ui.movMax.setText(str(self.ui.contBicepDer.maximum()))
            self.ui.movMin.setText(str(self.ui.contBicepDer.minimum()))
            self.ui.ultimoMov.setText("BicepDer: " + str(i))
            self.threadServos.start()
    def DeltoideDer(self, i):
        self.cambiarListas('DeltoideDer', i)
        if ~self.threadServos.isRunning():
            self.threadServos.valores("DeltoideDer", i)
            self.ui.movMax.setText(str(self.ui.contDeltoideDer.maximum()))
            self.ui.movMin.setText(str(self.ui.contDeltoideDer.minimum()))
            self.ui.ultimoMov.setText("DeltoideDer: " + str(i))
            self.threadServos.start()
    def AntebrazoDer(self, i):
        self.cambiarListas('AntebrazoDer', i)
        if ~self.threadServos.isRunning():
            self.threadServos.valores("AntebrazoDer", i)
            self.ui.movMax.setText(str(self.ui.contAntebrazoDer.maximum()))
            self.ui.movMin.setText(str(self.ui.contAntebrazoDer.minimum()))
            self.ui.ultimoMov.setText("AntebrazoDer: " + str(i))
            self.threadServos.start()
    def CodoDer(self, i):
        self.cambiarListas('CodoDer', i)
        if ~self.threadServos.isRunning():
            self.threadServos.valores("CodoDer", i)
            self.ui.movMax.setText(str(self.ui.contCodoDer.maximum()))
            self.ui.movMin.setText(str(self.ui.contCodoDer.minimum()))
            self.ui.ultimoMov.setText("CodoDer: " + str(i))
            self.threadServos.start()
    def MunecaDer(self, i):
        self.cambiarListas('MunecaDer', i)
        if ~self.threadServos.isRunning():
            self.threadServos.valores("MunecaDer", i)
            self.ui.movMax.setText(str(self.ui.contMunecaDer.maximum()))
            self.ui.movMin.setText(str(self.ui.contMunecaDer.minimum()))
            self.ui.ultimoMov.setText("MunecaDer: " + str(i))
            self.threadServos.start()
    def IndiceDer(self, i):
        self.cambiarListas('IndiceDer', i)
        if ~self.threadServos.isRunning():
            self.threadServos.valores("IndiceDer", i)
            self.ui.movMax.setText(str(self.ui.contIndiceDer.maximum()))
            self.ui.movMin.setText(str(self.ui.contIndiceDer.minimum()))
            self.ui.ultimoMov.setText("IndiceDer: " + str(i))
            self.threadServos.start()
    def MedioDer(self, i):
        self.cambiarListas('MedioDer', i)
        if ~self.threadServos.isRunning():
            self.threadServos.valores("MedioDer", i)
            self.ui.movMax.setText(str(self.ui.contMedioDer.maximum()))
            self.ui.movMin.setText(str(self.ui.contMedioDer.minimum()))
            self.ui.ultimoMov.setText("MedioDer: " + str(i))
            self.threadServos.start()
    def AnularDer(self, i):
        self.cambiarListas('AnularDer', i)
        if ~self.threadServos.isRunning():
            self.threadServos.valores("AnularDer", i)
            self.ui.movMax.setText(str(self.ui.contAnularDer.maximum()))
            self.ui.movMin.setText(str(self.ui.contAnularDer.minimum()))
            self.ui.ultimoMov.setText("AnularDer: " + str(i))
            self.threadServos.start()
    def MeniqueDer(self, i):
        self.cambiarListas('MeniqueDer', i)
        if ~self.threadServos.isRunning():
            self.threadServos.valores("MeniqueDer", i)
            self.ui.movMax.setText(str(self.ui.contMeniqueDer.maximum()))
            self.ui.movMin.setText(str(self.ui.contMeniqueDer.minimum()))
            self.ui.ultimoMov.setText("MeniqueDer: " + str(i))
            self.threadServos.start()
    def PulgarDer(self, i):
        self.cambiarListas('PulgarDer', i)
        if ~self.threadServos.isRunning():
            self.threadServos.valores("PulgarDer", i)
            self.ui.movMax.setText(str(self.ui.contPulgarDer.maximum()))
            self.ui.movMin.setText(str(self.ui.contPulgarDer.minimum()))
            self.ui.ultimoMov.setText("PulgarDer: " + str(i))
            self.threadServos.start()
    def cambiarListas(self, key, valor):
        if self.ui.selectorMovimiento.currentText() == "Movimiento1":
            Ui_Servos['movimiento1'][key] = valor
        elif self.ui.selectorMovimiento.currentText() == "Movimiento1":
            Ui_Servos['movimiento2'][key] = valor
        elif self.ui.selectorMovimiento.currentText() == "Movimiento3":
            Ui_Servos['movimiento3'][key] = valor

    def finalizarThread(self, valor):
        if valor == "fin":
            self.threadServos.wait()
    def inicializarAcciones(self):
        #Inicializar el giro
        for x in range(self.ui.selectorAcciones.count()):
            self.ui.selectorAcciones.removeItem(0)
        try:
            data = ""
            with open('angulosservos.json', 'r') as f:
                data = f.read()
            self.datos = json.loads(data)
            for x in self.datos:
                self.ui.selectorAcciones.addItem(x.get("nombre"))
        except FileNotFoundError:
            QtWidgets.QMessageBox.information(self, "Error", "Inicializando el archivo con los datos del servo")
            diccionarioJSON = {
            'nombre': 'Default',
            'movimiento1': Ui_Servos.movimiento1,
            'movimiento2': Ui_Servos.movimiento2,
            'movimiento3': Ui_Servos.movimiento3
            }
            self.datos.append(diccionarioJSON)
            datosplanos = json.dumps(self.datos)   #Convertir a String como JSON
            f = open('angulosservos.json', 'x')
            f.write(datosplanos)
            f.close()
            self.ui.selectorAcciones.addItem("Default")
        self.ui.selectorAcciones.addItem("Nuevo")
        self.ui.selectorAcciones.setCurrentIndex(self.ui.selectorAcciones.count() -1)

    def guardar(self):
        nMovimento = self.ui.nombreMovimiento.text()
        if self.ui.botonGuardar.text() == "Añadir":
            if self.ui.nombreMovimiento.text() == "":
                QtWidgets.QMessageBox.warning(self, "Error", "Indique un nombre de movimiento e intente de nuevo por favor")
                return
            diccionarioJSON = {
            'nombre': self.ui.nombreMovimiento.text(),
            'movimiento1': Ui_Servos.movimiento1,
            'movimiento2': Ui_Servos.movimiento2,
            'movimiento3': Ui_Servos.movimiento3
            }
            print(diccionarioJSON)
            self.datos.append(diccionarioJSON)
        else: #Modificar
            for x in self.datos:
                if x['nombre'] == self.ui.selectorAcciones.currentText():
                    x['nombre'] = self.ui.nombreMovimiento.text()
                    x['movimiento1'] = Ui_Servos.movimiento1,
                    x['movimiento2'] =  Ui_Servos.movimiento2,
                    x['movimiento3'] =  Ui_Servos.movimiento3
                    break

        datosplanos = json.dumps(self.datos)   #Convertir a String como JSON
        f = open('angulosservos.json', 'w')
        f.write(datosplanos)
        f.close()
        if self.ui.botonGuardar.text() == "Añadir":
            self.inicializarAcciones()
            self.cambiarValor(nMovimento)
            self.ui.selectorMovimiento.setCurrentIndex(0)
            cambioindex = self.ui.selectorAcciones.findText(nMovimento)
            self.ui.selectorAcciones.setCurrentIndex(cambioindex)

    def eliminarAcciones(self):
        if self.ui.selectorAcciones.currentText() == "Default":
            QtWidgets.QMessageBox.warning(self, "Error", "No se puede eliminar la accion por Default")
        elif self.ui.selectorAcciones.currentText() == "Nuevo":
            QtWidgets.QMessageBox.warning(self, "Error", "No se puede eliminar la accion por Nuevo")
        else:
            respuesta = QtWidgets.QMessageBox.question(self, "Continuar", "Desea eliminar el registro")
            if respuesta == QtWidgets.QMessageBox.Yes:
                print(type(self.datos))
                datoeliminar = {}
                for x in self.datos:
                    if x['nombre'] == self.ui.selectorAcciones.currentText():
                        datoeliminar = x
                        break
                self.datos.remove(datoeliminar)
                datosplanos = json.dumps(self.datos)   #Convertir a String como JSON
                f = open('angulosservos.json', 'w')
                f.write(datosplanos)
                f.close()
                self.inicializarAcciones()


    def cambiarMovimiento(self, valor):
        movimiento = {}
        if self.ui.selectorMovimiento.currentText() == "Movimiento1":
            movimiento = Ui_Servos.movimiento1
        elif self.ui.selectorMovimiento.currentText() == "Movimiento2":
            movimiento = Ui_Servos.movimiento2
        elif self.ui.selectorMovimiento.currentText() == "Movimiento3":
            movimiento = Ui_Servos.movimiento3
        print('22222222222222 movimiento 222222222222222')
        print(movimiento)
        self.ui.contBicepIzq.setValue(movimiento['BicepIzq'])
        self.ui.contDeltoideIzq.setValue(movimiento['DeltoideIzq'])
        self.ui.contAntebrazoIzq.setValue(movimiento['AntebrazoIzq'])
        self.ui.contCodoIzq.setValue(movimiento['CodoIzq'])
        self.ui.contMunecaIzq.setValue(movimiento['MunecaIzq'])
        self.ui.contIndiceIzq.setValue(movimiento['IndiceIzq'])
        self.ui.contMedioIzq.setValue(movimiento['MedioIzq'])
        self.ui.contAnularIzq.setValue(movimiento['AnularIzq'])
        self.ui.contMeniqueIzq.setValue(movimiento['MeniqueIzq'])
        self.ui.contPulgarIzq.setValue(movimiento['PulgarIzq'])
        self.ui.contBicepDer.setValue(movimiento['BicepDer'])
        self.ui.contDeltoideDer.setValue(movimiento['DeltoideDer'])
        self.ui.contAntebrazoDer.setValue(movimiento['AntebrazoDer'])
        self.ui.contCodoDer.setValue(movimiento['CodoDer'])
        self.ui.contMunecaDer.setValue(movimiento['MunecaDer'])
        self.ui.contIndiceDer.setValue(movimiento['IndiceDer'])
        self.ui.contMedioDer.setValue(movimiento['MedioDer'])
        self.ui.contAnularDer.setValue(movimiento['AnularDer'])
        self.ui.contMeniqueDer.setValue(movimiento['MeniqueDer'])
        self.ui.contPulgarDer.setValue(movimiento['PulgarDer'])
        if ~self.threadTodosServos.isRunning():
            self.threadTodosServos.inicializarAcciones(movimiento, objetoControlServos.posicionactual)
            self.threadTodosServos.start()

    def cambiarValor(self, texto):
        if texto == "Nuevo":
            self.ui.botonGuardar.setText("Añadir")
            self.ui.nombreMovimiento.setText("")
            self.ui.contBicepIzq.setValue(0)
            self.ui.contDeltoideIzq.setValue(0)
            self.ui.contAntebrazoIzq.setValue(0)
            self.ui.contCodoIzq.setValue(0)
            self.ui.contMunecaIzq.setValue(0)
            self.ui.contIndiceIzq.setValue(0)
            self.ui.contMedioIzq.setValue(0)
            self.ui.contAnularIzq.setValue(0)
            self.ui.contMeniqueIzq.setValue(0)
            self.ui.contPulgarIzq.setValue(0)
            self.ui.contBicepDer.setValue(0)
            self.ui.contDeltoideDer.setValue(0)
            self.ui.contAntebrazoDer.setValue(0)
            self.ui.contCodoDer.setValue(0)
            self.ui.contMunecaDer.setValue(0)
            self.ui.contIndiceDer.setValue(0)
            self.ui.contMedioDer.setValue(0)
            self.ui.contAnularDer.setValue(0)
            self.ui.contMeniqueDer.setValue(0)
            self.ui.contPulgarDer.setValue(0)
            self.ui.selectorMovimiento.setCurrentIndex(0)
        else:
            self.ui.botonGuardar.setText("Modificar")
        movimiento = {}
        for x in self.datos:
            if x['nombre'] == texto:
                if ~self.threadTodosServos.isRunning():
                    print("Texto: " + self.ui.selectorMovimiento.currentText())
                    if self.ui.selectorMovimiento.currentText() == "Movimiento1":
                        movimiento = Ui_Servos.movimiento1
                    elif self.ui.selectorMovimiento.currentText() == "Movimiento2":
                        movimiento = Ui_Servos.movimiento2
                    elif self.ui.selectorMovimiento.currentText() == "Movimiento3":
                        movimiento = Ui_Servos.movimiento3
                    self.threadTodosServos.inicializarAcciones(movimiento, objetoControlServos.posicionactual)
                    self.threadTodosServos.start()
                print(movimiento)
                self.ui.nombreMovimiento.setText(texto)
                self.ui.contBicepIzq.setValue(movimiento['BicepIzq'])
                self.ui.contDeltoideIzq.setValue(movimiento['DeltoideIzq'])
                self.ui.contAntebrazoIzq.setValue(movimiento['AntebrazoIzq'])
                self.ui.contCodoIzq.setValue(movimiento['CodoIzq'])
                self.ui.contMunecaIzq.setValue(movimiento['MunecaIzq'])
                self.ui.contIndiceIzq.setValue(movimiento['IndiceIzq'])
                self.ui.contMedioIzq.setValue(movimiento['MedioIzq'])
                self.ui.contAnularIzq.setValue(movimiento['AnularIzq'])
                self.ui.contMeniqueIzq.setValue(movimiento['MeniqueIzq'])
                self.ui.contPulgarIzq.setValue(movimiento['PulgarIzq'])
                self.ui.contBicepDer.setValue(movimiento['BicepDer'])
                self.ui.contDeltoideDer.setValue(movimiento['DeltoideDer'])
                self.ui.contAntebrazoDer.setValue(movimiento['AntebrazoDer'])
                self.ui.contCodoDer.setValue(movimiento['CodoDer'])
                self.ui.contMunecaDer.setValue(movimiento['MunecaDer'])
                self.ui.contIndiceDer.setValue(movimiento['IndiceDer'])
                self.ui.contMedioDer.setValue(movimiento['MedioDer'])
                self.ui.contAnularDer.setValue(movimiento['AnularDer'])
                self.ui.contMeniqueDer.setValue(movimiento['MeniqueDer'])
                self.ui.contPulgarDer.setValue(movimiento['PulgarDer'])
                break

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    VentanaServos = QtWidgets.QDialog()
    ui = Ui_Servos()
    ui.show()
    sys.exit(app.exec_())
