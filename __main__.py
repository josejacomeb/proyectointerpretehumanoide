#!/usr/bin/env python3

from PyQt5.QtWidgets import QDialog
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from ventanaprincipal import Ui_Dialog
import cv2
import numpy as np
import pyzbar.pyzbar as pyzbar
import imagenes_rc
from controlservos import *
from controlpantalla import *
from bluetooth import *
import json
if platform.machine() == 'armv7l':
    import RPi.GPIO as GPIO

cap = cv2.VideoCapture(0)
hasFrame,frame = cap.read()

class accionServos(QThread):
    senalFin =  pyqtSignal(str)
    def __init__(self, pasos=10):
        QThread.__init__(self)
        self.pasos = pasos
        self.objetoControlServos = ControlServos(delay=0.1) #Inicializo el objeto
    def run(self):
        print("-----* Inicializa while *---------")
        self.senalFin.emit("inicio")
        self.finalizaracciones = False
        while self.finalizaracciones == False:
            self.finalizaracciones = True
            movimientoDeseado = {
            'nombre': 'MovimientoDeseado',
            'BicepIzq': 0,
            'DeltoideIzq': 0,
            'AntebrazoIzq': 0,
            'CodoIzq': 0,
            'MunecaIzq': 0,
            'IndiceIzq': 0,
            'MedioIzq': 0,
            'AnularIzq': 0,
            'MeniqueIzq': 0,
            'PulgarIzq': 0,
            'BicepDer': 0,
            'DeltoideDer': 0,
            'AntebrazoDer': 0,
            'CodoDer': 0,
            'MunecaDer': 0,
            'IndiceDer': 0,
            'MedioDer': 0,
            'AnularDer': 0,
            'MeniqueDer': 0,
            'PulgarDer': 0
            }
            for i in self.datos:
                if i['finalizado'] == False:
                    i['actual'] += i['incremento']
                movimientoDeseado[i['movimiento']] = i['actual']
                if int(i['actual']) == int(i['deseado']):
                    i['finalizado'] = True
                else:
                    if int(i['actual']) <= int(i['deseado'] + abs(i['incremento']/2)) and int(i['actual']) >= int(i['deseado']- abs(i['incremento']/2)):
                        i['finalizado'] = True
                self.finalizaracciones &= i['finalizado']
            if ~self.finalizaracciones:
                self.objetoControlServos.moverGrupo(movimientoDeseado)
            else:
                print(self.datos)
                print("***Finalizado***")
            print("Finalizado? " + str(self.finalizaracciones));
        print("-----* Fin while *---------")
        self.senalFin.emit("fin")
    def __del__(self):
        self.wait()

    def inicializarAcciones(self, deseado, actual, default = False):
        '''
        Inicializa el vector de datos para la ejecucion en el hilo Principal
        '''
        self.datos = []
        llaves = deseado.keys()
        for x in llaves:
            if x != 'nombre':
                incrementoservos = 0
                if (deseado[x] - actual[x])/self.pasos <= 2:
                    incrementoservos = (deseado[x] - actual[x])
                else:
                    incrementoservos = (deseado[x] - actual[x])/self.pasos
                diccionario = {
                'movimiento': x,
                'actual': actual[x],
                'deseado': deseado[x],
                'finalizado': False,
                'incremento': incrementoservos
                }
                self.datos.append(diccionario)
class accionOLED(QThread):
    senalFin =  pyqtSignal(str)
    def __init__(self, texto):
        QThread.__init__(self)
        self.objetoControlPantalla = ControlPantalla(delay=1)
        print("Finalizo constructor")
        print("Texto: " + texto)
    def run(self):
        print("-----* Inicializa while *---------")
        for i in self.palabra:
            self.objetoControlPantalla.mostrarLetra(i)
        self.objetoControlPantalla.mostrarLetra('ingenieria')

        self.senalFin.emit("fin")
    def __del__(self):
        self.wait()

    def inicializarPalabra(self, palabra):
        self.palabra = palabra

class accionBT(QThread):
    senalBT =  pyqtSignal(str)
    def __init__(self):
        QThread.__init__(self)
        self.server_sock=BluetoothSocket( RFCOMM )
        self.server_sock.bind(("",PORT_ANY))
        self.server_sock.listen(1)

        self.port = self.server_sock.getsockname()[1]

        uuid = "94f39d29-7d6d-437d-973b-fba39e49d4ee"

        advertise_service( self.server_sock, "SampleServer",
                           service_id = uuid,
                           service_classes = [ uuid, SERIAL_PORT_CLASS ],
                           profiles = [ SERIAL_PORT_PROFILE ],
        #                   protocols = [ OBEX_UUID ]
                            )

        print("Esperando conexión BT %d" % self.port)
    def run(self):
        self.client_sock, self.client_info = self.server_sock.accept()
        print("Accepted connection from ", self.client_info)

        try:
            while True:
                data = self.client_sock.recv(1024)
                if len(data) == 0: break
                print("received [%s]" % data)
                self.senalBT.emit(data.decode("utf-8").replace("\r\n",""))
        except IOError:
            pass
        print("disconnected")
    def __del__(self):
        self.client_sock.close()
        self.server_sock.close()
        print("all done")



class Thread(QThread):
    changePixmap = pyqtSignal(QImage)
    senalPunch =  pyqtSignal(str)
    cambioROI = pyqtSignal(QImage)
    #imagen = np.
    def run(self):
        while True:
            roi = np.zeros((64, 64, 3), dtype = "uint8")
            hasFrame,frame = cap.read()
            if not hasFrame:
                break
            decodedObjects = pyzbar.decode(frame)
            if len(decodedObjects):
                zbarData = decodedObjects[0].data
            else:
                zbarData=b''
            for decodedObject in decodedObjects:
                points = decodedObject.polygon

                # If the points do not form a quad, find convex hull
                if len(points) > 4 :
                    hull = cv2.convexHull(np.array([point for point in points], dtype=np.float32))
                    hull = list(map(tuple, np.squeeze(hull)))
                else :
                    hull = points
                # Number of points in the convex hull
                n = len(hull)
                if len(points) == 4:
                    puntox = hull[0].x
                    puntoy  = hull[0].y
                    ancho = 0
                    alto = 0
                    minx = 1000
                    miny = 1000
                    for j in range(0,n):
                        if abs(hull[j].x - puntox) > ancho:
                            ancho = abs(hull[j].x - puntox)
                        if abs(hull[j].y - puntoy) > alto:
                            alto = abs(hull[j].y - puntoy)
                        if minx > abs(hull[j].x):
                            minx = abs(hull[j].x)
                        if miny > abs(hull[j].y):
                            miny = abs(hull[j].y)
                        puntox = hull[j].x
                        puntoy = hull[j].y
                    roi = frame[miny: miny + alto, minx:minx + ancho]
                # Draw the convext hull
                for j in range(0,n):
                    cv2.line(frame, hull[j], hull[ (j+1) % n], (255,0,255), 3)


            rgbImage = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            convertToQtFormat = QImage(rgbImage.data, rgbImage.shape[1], rgbImage.shape[0], QImage.Format_RGB888)
            p = convertToQtFormat.scaled(270, 200, Qt.KeepAspectRatio)
            self.changePixmap.emit(p)
            roiCV = cv2.cvtColor(roi, cv2.COLOR_BGR2RGB)
            ROIqt = QImage(roiCV.data, roiCV.shape[1], roiCV.shape[0], QImage.Format_RGB888)
            pROIQT = ROIqt.scaled(64, 64, Qt.KeepAspectRatio)
            self.cambioROI.emit(pROIQT)
            self.senalPunch.emit(zbarData.decode("latin1"))

class Ui_VentanaPrincipal(QDialog, Ui_Dialog):# Number of points in the convex hull
    anterior = ""
    def __init__(self, MotorA1=16, MotorA2=20, MotorB1=19, MotorB2=26):
        super(Ui_VentanaPrincipal, self).__init__()
        # Set up the user interface from Designer.
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.th = Thread(VentanaPrincipal)
        self.MotorA1 = MotorA1
        self.MotorA2 = MotorA2
        self.MotorB1 = MotorB1
        self.MotorB2 = MotorB2
        if platform.machine() == 'armv7l':
            #GPIO.setmode(GPIO.BOARD)
            GPIO.setup(self.MotorA1, GPIO.OUT)
            GPIO.setup(self.MotorA2, GPIO.OUT)
            GPIO.setup(self.MotorB1, GPIO.OUT)
            GPIO.setup(self.MotorB2, GPIO.OUT)
        self.th.changePixmap.connect(lambda p: self.ui.videoStream.setPixmap(QPixmap(p)))
        self.th.cambioROI.connect(lambda e: self.ui.label_5.setPixmap(QPixmap(e)))
        self.th.senalPunch.connect(self.cambiar)
        self.threadServos = accionServos()
        self.threadServos.senalFin.connect(self.finalizarThread)
        self.threadOLED = accionOLED("hola")
        self.threadOLED.senalFin.connect(self.finalizarOLED)
        self.threadBT = accionBT()
        self.threadBT.start()
        self.threadBT.senalBT.connect(self.cambiarBT)
        self.th.start()
        self.ui.botonReiniciarPosicion.clicked.connect(self.reiniciarPosicion)
        datosdeseados = self.obtenerValorJSON("Default")
        if ~self.threadServos.isRunning():
            self.threadServos.inicializarAcciones(datosdeseados, self.threadServos.objetoControlServos.posicionactual, True)
            self.threadServos.start()

    def reiniciarPosicion(self):
        datosdeseados = self.obtenerValorJSON("Default")
        if ~self.threadServos.isRunning():
            time.sleep(self.objetoControlServos.delay*10)
            self.threadServos.inicializarAcciones(datosdeseados, self.threadServos.objetoControlServos.posicionactual, True)
            self.threadServos.start()
    def obtenerValorJSON(self, valor):
        print("******* Nuevo valor *******")
        with open('angulosservos.json', 'r') as f:
            data = f.read()
        self.datos = json.loads(data)
        datosdeseados = {}
        for x in self.datos:
            print("Bool: " + str(x.get("nombre").lower() == valor.lower()) + " vector: " +  str(x.get("nombre").lower()) + " " + str(type(x.get("nombre").lower())) + " valor: " + str(valor.lower())+ " " + str(type(valor.lower())))
            if x.get("nombre").lower() == valor.lower():
                datosdeseados = x
                break
        return datosdeseados

    def cambiarBT(self, valor):
        adelante = "motoradelante"
        atras = "motoratras"
        izquierda = "motorizquierda"
        derecha = "motorderecha"
        if valor == adelante:
            if platform.machine() == 'armv7l':
                GPIO.output(self.MotorA1, True)
                GPIO.output(self.MotorA2, False)
                GPIO.output(self.MotorB1, True)
                GPIO.output(self.MotorB1, False)
        elif valor == atras:
            if platform.machine() == 'armv7l':
                GPIO.output(self.MotorA1, False)
                GPIO.output(self.MotorA2, True)
                GPIO.output(self.MotorB1, False)
                GPIO.output(self.MotorB1, True)
        elif valor == izquierda:
            if platform.machine() == 'armv7l':
                GPIO.output(self.MotorA1, False)
                GPIO.output(self.MotorA2, True)
                GPIO.output(self.MotorB1, True)
                GPIO.output(self.MotorB1, False)
        elif valor == derecha:
            if platform.machine() == 'armv7l':
                GPIO.output(self.MotorA1, True)
                GPIO.output(self.MotorA2, False)
                GPIO.output(self.MotorB1, False)
                GPIO.output(self.MotorB1, True)
        else:
            if self.anterior != valor:
                print(type(valor))
                datosdeseados = self.obtenerValorJSON(valor)
                if datosdeseados != {}:
                    if ~self.threadServos.isRunning():
                        self.threadServos.inicializarAcciones(datosdeseados, self.threadServos.objetoControlServos.posicionactual)
                        self.threadServos.start()
                    if ~self.threadOLED.isRunning():
                        self.threadOLED.inicializarPalabra(str(valor))
                        self.threadOLED.start()
                else:
                    print("Datos no reconocidos")
            self.anterior = valor

        self.ui.palabraDetectada.setText("BT: " + valor)
    def cambiar(self, valor):
        if valor != "":
            self.ui.palabraDetectada.setText(valor)
        if valor != "":
            if self.anterior != valor:
                datosdeseados = self.obtenerValorJSON(valor)
                if datosdeseados != {}:
                    print(datosdeseados)
                    if ~self.threadServos.isRunning():
                        self.threadServos.inicializarAcciones(datosdeseados, self.threadServos.objetoControlServos.posicionactual)
                        self.threadServos.start()
                    if ~self.threadOLED.isRunning():
                        self.threadOLED.inicializarPalabra(str(valor))
                        self.threadOLED.start()
                else:
                    print("Datos no reconocidos")
            self.anterior = valor
    def finalizarThread(self, valor):
        if valor == "fin":
            self.threadServos.wait()
    def finalizarOLED(self, valor):
        if valor == "fin":
            self.threadOLED.wait()


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    VentanaPrincipal = QtWidgets.QDialog()
    ui = Ui_VentanaPrincipal()
    ui.show()
    sys.exit(app.exec_())
