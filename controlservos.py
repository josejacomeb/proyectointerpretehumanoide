'''
    Programa para controlar los servos a través del programa
'''
import platform
if platform.machine() == 'armv7l':
    from adafruit_servokit import ServoKit
import time

class ControlServos():
    def __init__(self, canales=16, dir1 = 0x41, dir2 = 0x40, delay = 0.75, pwMG995 = [600, 2000], pwSG90 = [500, 2400], npasos = 3):
        self.posicionactual = {
        'nombre': 'PosicionActual',
        'BicepIzq': 0,
        'DeltoideIzq': 0,
        'AntebrazoIzq': 0,
        'CodoIzq': 0,
        'MunecaIzq': 0,
        'IndiceIzq': 0,
        'MedioIzq': 0,
        'AnularIzq': 0,
        'MeniqueIzq': 0,
        'PulgarIzq': 0,
        'BicepDer': 0,
        'DeltoideDer': 0,
        'AntebrazoDer': 0,
        'CodoDer': 0,
        'MunecaDer': 0,
        'IndiceDer': 0,
        'MedioDer': 0,
        'AnularDer': 0,
        'MeniqueDer': 0,
        'PulgarDer': 0,
        }
        if platform.machine() == 'armv7l':
            self.npasos = npasos
            self.servosTorso = ServoKit(channels=canales, address=dir1)
            self.servosManos = ServoKit(channels=canales, address=dir2)
            self.valoresServos = {
            'nombre': 'valoresServos',
            'BicepIzq': self.servosTorso.servo[0],
            'DeltoideIzq': self.servosTorso.servo[1],
            'BicepDer': self.servosTorso.servo[2],
            'DeltoideDer': self.servosTorso.servo[3],
            'AntebrazoIzq': self.servosTorso.servo[4],
            'AntebrazoDer': self.servosTorso.servo[5],
            'CodoIzq': self.servosTorso.servo[6],
            'CodoDer': self.servosTorso.servo[7],
            'MunecaIzq': self.servosManos.servo[0],
            'IndiceIzq': self.servosManos.servo[1],
            'MedioIzq': self.servosManos.servo[2],
            'AnularIzq': self.servosManos.servo[3],
            'MeniqueIzq': self.servosManos.servo[4],
            'PulgarIzq': self.servosManos.servo[5],
            'IndiceDer': self.servosManos.servo[6],
            'MedioDer': self.servosManos.servo[7],
            'AnularDer': self.servosManos.servo[8],
            'MeniqueDer': self.servosManos.servo[9],
            'PulgarDer': self.servosManos.servo[10],
            'MunecaDer': self.servosManos.servo[11]
            }
            #Establece valores correctos de Pulsw Widht Range para los servos
            self.valoresServos['BicepIzq'].set_pulse_width_range(pwMG995[0], pwMG995[1])
            self.valoresServos['BicepDer'].set_pulse_width_range(pwMG995[0], pwMG995[1])
            self.valoresServos['DeltoideIzq'].set_pulse_width_range(pwMG995[0], pwMG995[1])
            self.valoresServos['DeltoideDer'].set_pulse_width_range(pwMG995[0], pwMG995[1])
            self.valoresServos['CodoIzq'].set_pulse_width_range(pwMG995[0], pwSG90[1])
            self.valoresServos['CodoDer'].set_pulse_width_range(pwMG995[0], pwSG90[1])
            self.valoresServos['AntebrazoIzq'].set_pulse_width_range(pwMG995[0], pwSG90[1])
            self.valoresServos['AntebrazoDer'].set_pulse_width_range(pwMG995[0], pwSG90[1])
            self.valoresServos['MunecaIzq'].set_pulse_width_range(pwSG90[0], pwSG90[1])
            self.valoresServos['MunecaDer'].set_pulse_width_range(pwSG90[0], pwSG90[1])
            self.valoresServos['IndiceIzq'].set_pulse_width_range(pwSG90[0], pwSG90[1])
            self.valoresServos['IndiceDer'].set_pulse_width_range(pwSG90[0], pwSG90[1])
            self.valoresServos['MedioIzq'].set_pulse_width_range(pwSG90[0], pwSG90[1])
            self.valoresServos['MedioDer'].set_pulse_width_range(pwSG90[0], pwSG90[1])
            self.valoresServos['AnularIzq'].set_pulse_width_range(pwSG90[0], pwSG90[1])
            self.valoresServos['AnularDer'].set_pulse_width_range(pwSG90[0], pwSG90[1])
            self.valoresServos['MeniqueIzq'].set_pulse_width_range(pwSG90[0], pwSG90[1])
            self.valoresServos['MeniqueDer'].set_pulse_width_range(pwSG90[0], pwSG90[1])
            self.valoresServos['PulgarIzq'].set_pulse_width_range(pwSG90[0], pwSG90[1])
            self.valoresServos['PulgarDer'].set_pulse_width_range(pwSG90[0], pwSG90[1])
        self.delay = delay
    '''def prueba(self, movimientos):
        llaves = movimientos.keys()
        for i in llaves:
            if i != "nombre":
                self.posicionactual[i] = movimientos[i]
        print("****Posiciones control servo****")
        print(self.posicionactual)
        time.sleep(self.delay)'''
    def moverIndividual(self, ID, angulo):
        if platform.machine() == 'armv7l':
            diferenciaangulo = (int(angulo) - self.posicionactual[ID])/self.npasos
            print("angulo: " + str(angulo))
            if diferenciaangulo < 10.0:
                self.valoresServos[ID].angle = int(angulo)
                time.sleep(self.delay)
                self.posicionactual[ID] = int(angulo)
            else:
                print(self.posicionactual[ID])
                print(angulo)
                print(diferenciaangulo)
                for i in range(int(self.posicionactual[ID]), angulo, int(diferenciaangulo)):
                    self.valoresServos[ID].angle = int(i)
                    time.sleep(self.delay/self.npasos)
                self.valoresServos[ID].angle = int(angulo)
                time.sleep(self.delay/self.npasos)
                self.posicionactual[ID] = int(angulo)
        else:
            print( str(ID) + ": " + str(angulo) + "º")


    def moverGrupo(self, movimientos):
        print("Acabo delay")
        llaves = movimientos.keys()
        for i in llaves:
            if i != "nombre":
                self.posicionactual[i] = movimientos[i]
                if platform.machine() == 'armv7l':
                    print(i + str(int(movimientos[i])))
                    self.valoresServos[i].angle = int(movimientos[i])
                else:
                    print(str(i) + ": " + str(int(movimientos[i])) + "º")

        time.sleep(self.delay)
