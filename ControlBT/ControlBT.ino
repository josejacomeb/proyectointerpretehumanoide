char c = '0';
#define AA 3
#define AB 2
#define BA 5
#define BB 4

void setup() {
  Serial.begin(9600);
  pinMode(AA, OUTPUT);
  pinMode(AB, OUTPUT);
  pinMode(BA, OUTPUT);
  pinMode(BB, OUTPUT);
  digitalWrite(AA, LOW);
  digitalWrite(AB, LOW);
  digitalWrite(BA, LOW);
  digitalWrite(BB, LOW);
}

void loop() {
  if (Serial.available()) {
    c = Serial.read();
    if (c == 'a') { //Adelante
      digitalWrite(AA, HIGH);
      digitalWrite(AB, LOW);
      digitalWrite(BA, HIGH);
      digitalWrite(BB, LOW);
    }
    else if (c == 'i') { //Izquierda
      digitalWrite(AA, HIGH);
      digitalWrite(AB, LOW);
      digitalWrite(BA, LOW);
      digitalWrite(BB, HIGH);
    }
    else if (c == 'd') { //Derecha
      digitalWrite(AA, LOW);
      digitalWrite(AB, HIGH);
      digitalWrite(BA, HIGH);
      digitalWrite(BB, LOW);
    }
    else if (c == 't') { //Atras
      digitalWrite(AA, LOW);
      digitalWrite(AB, HIGH);
      digitalWrite(BA, LOW);
      digitalWrite(BB, HIGH);
    }
    else if (c == 'p') { //Atras
      digitalWrite(AA, LOW);
      digitalWrite(AB, LOW);
      digitalWrite(BA, LOW);
      digitalWrite(BB, LOW);
    }

  }
}
